# "Hello World, REANA" RECAST workflow

This repository is a minimal "Hello World" level example of a RECAST workflow using [`recast-atlas`](https://github.com/recast-hep/recast-atlas) that uses the `reana` backend to deploy to CERN's REANA cluster.

## Prerequisites

Make sure you create a REANA account first at https://reana.cern.ch/ (and then record your [access token](https://reana.cern.ch/profile)).
