#!/bin/bash

# Force working inside of a Python virtual environment
export PIP_REQUIRE_VIRTUALENV=true

# Install Python dependencies
python -m pip --quiet install --upgrade pip setuptools wheel
python -m pip --quiet install --upgrade 'recast-atlas[reana]>=0.3.0' coolname

# Authenticate
# Set these variables to your personal secret values
export RECAST_AUTH_USERNAME=#USERNAME
export RECAST_AUTH_PASSWORD=#SECRET
export RECAST_AUTH_TOKEN=#SECRET

eval "$(recast auth setup -a ${RECAST_AUTH_USERNAME} -a ${RECAST_AUTH_PASSWORD} -a ${RECAST_AUTH_TOKEN} -a default)"
eval "$(recast auth write --basedir authdir)"

# Unset after authdir creation to avoid secrets in memory
unset RECAST_AUTH_USERNAME
unset RECAST_AUTH_PASSWORD
unset RECAST_AUTH_TOKEN

# Values taken from https://reana.cern.ch/profile
export REANA_SERVER_URL=https://reana.cern.ch
export REANA_ACCESS_TOKEN=#SECRET

echo -e "\n# reana-client info"
reana-client info

echo -e "\n# reana-client ping"
reana-client ping

# Add the workflow
# Use a subshell for catalogue add to evaluate the shell export it produces
echo -e '\n# $(recast catalogue add "${PWD}")'
$(recast catalogue add "${PWD}")

echo -e '\n# echo "${RECAST_ATLAS_CATALOGUE}"'
echo "${RECAST_ATLAS_CATALOGUE}"

echo -e "\n# recast catalogue ls"
recast catalogue ls

echo -e "\n# recast catalogue describe examples/helloworld-reana"
recast catalogue describe examples/helloworld-reana

echo -e "\n# recast catalogue check examples/helloworld-reana"
recast catalogue check examples/helloworld-reana

# Run the workflow
# The default --example value is 'default'
_default_tag=$(coolname 2)
echo -e '\n# recast submit --backend reana --tag '"helloworld-reana-${_default_tag}"' examples/helloworld-reana'
recast submit \
    --backend reana \
    --tag "helloworld-reana-${_default_tag}" \
    examples/helloworld-reana

sleep 2

echo -e "\n# reana-client status --workflow recast-helloworld-reana-${_default_tag}"
reana-client status \
    --workflow "recast-helloworld-reana-${_default_tag}"

echo -e "\n# After 'reana-client status' (above) shows that the workflow has finished check the logs with"
echo -e "# reana-client logs --workflow recast-helloworld-reana-${_default_tag}"
# echo -e "# and then download the result files with"
# echo -e "# reana-client download --workflow recast-helloworld-reana-${_default_tag} --output-directory output"
echo -e "\n# See 'reana-client --help' for more information"

# Cleanup authenticaion secrets and environmental variables
eval $(recast auth destroy)
